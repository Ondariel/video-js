import Vue from 'vue'
import Router from 'vue-router'
import SYVHome from '@/components/SYVHome'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'SYVHome',
      component: SYVHome
    },
  ]
})
